<?php

namespace Drupal\commerce_direct_tax_field;

use Drupal\Core\Datetime\DrupalDateTime;
use Drupal\commerce_tax\TaxRate;

/**
 * Represents a tax rate.
 */
class MonolithicTaxRate extends TaxRate {

  /**
   * Constructs a new MonolithicTaxRate instance.
   *
   * @param array $definition
   *   The definition.
   */
  public function __construct(array $definition) {
    foreach (['id', 'label', 'percentage'] as $required_property) {
      if (!isset($definition[$required_property]) || (empty($definition[$required_property]) && $definition[$required_property] !== '0')) {
        throw new \InvalidArgumentException(sprintf('Missing required property "%s".', $required_property));
      }
    }
    $definition['percentages'] = [
      [
        'number' => $definition['percentage'],
        'start_date' => 0,
      ],
    ];
    parent::__construct($definition);
  }

  /**
   * {@inheritdoc}
   */
  public function getPercentage(DrupalDateTime $date = NULL) {
    return $this->percentages[0];
  }

  /**
   * {@inheritdoc}
   */
  public function isDefault() {
    return TRUE;
  }

}
