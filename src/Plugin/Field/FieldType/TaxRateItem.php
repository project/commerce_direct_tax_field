<?php

namespace Drupal\commerce_direct_tax_field\Plugin\Field\FieldType;

use Drupal\Core\Field\FieldItemBase;
use Drupal\Core\Field\FieldStorageDefinitionInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\TypedData\DataDefinition;

/**
 * Defines the 'commerce_direct_tax_field_tax_rate' field type.
 *
 * @FieldType(
 *   id = "commerce_direct_tax_field_tax_rate",
 *   label = @Translation("Direct Tax Rate"),
 *   category = @Translation("Commerce"),
 *   default_widget = "commerce_direct_tax_field_direct_tax_rate",
 *   default_formatter = "direct_tax_rate_formatter"
 * )
 *
 * @DCG
 * If you are implementing a single value field type you may want to inherit
 * this class form some of the field type classes provided by Drupal core.
 * Check out /core/lib/Drupal/Core/Field/Plugin/Field/FieldType directory for a
 * list of available field type implementations.
 */
class TaxRateItem extends FieldItemBase {

  /**
   * {@inheritdoc}
   */
  public static function defaultStorageSettings() {
    $settings = ['tax_type' => ''];
    return $settings + parent::defaultStorageSettings();
  }

  /**
   * {@inheritdoc}
   */
  public function storageSettingsForm(array &$form, FormStateInterface $form_state, $has_data) {

    $taxTypes = \Drupal::entityTypeManager()->getStorage('commerce_tax_type')->loadMultiple();
    $options = [];
    foreach ($taxTypes as $taxType) {
      $options[$taxType->id()] = $taxType->label();
    }

    $element['tax_type'] = [
      '#type' => 'select',
      '#options' => $options,
      '#title' => $this->t('Tax type'),
      '#default_value' => $this->getSetting('tax_type'),
      '#disabled' => $has_data,
    ];

    return $element;
  }

  /**
   * {@inheritdoc}
   */
  public static function mainPropertyName() {
    return 'tax_rate';
  }

  /**
   * {@inheritdoc}
   */
  public function isEmpty() {
    $value = $this->get('tax_rate')->getValue();
    return $value === NULL || $value === '_none';
  }

  /**
   * {@inheritdoc}
   */
  public static function propertyDefinitions(FieldStorageDefinitionInterface $field_definition) {

    // @DCG
    // See /core/lib/Drupal/Core/TypedData/Plugin/DataType directory for
    // available data types.
    $properties['tax_rate'] = DataDefinition::create('string')
      ->setLabel(t('Tax rate'))
      ->setRequired(TRUE);

    return $properties;
  }

  /**
   * {@inheritdoc}
   */
  public static function schema(FieldStorageDefinitionInterface $field_definition) {

    $columns = [
      'tax_rate' => [
        'type' => 'varchar',
        'not null' => FALSE,
        'description' => 'Tax rate.',
        'length' => 255,
      ],
    ];

    $schema = [
      'columns' => $columns,
      // @DCG Add indexes here if necessary.
    ];

    return $schema;
  }

}
