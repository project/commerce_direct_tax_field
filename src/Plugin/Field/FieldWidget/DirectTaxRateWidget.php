<?php

namespace Drupal\commerce_direct_tax_field\Plugin\Field\FieldWidget;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\WidgetBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Field\FieldDefinitionInterface;

/**
 * Defines the 'commerce_direct_tax_field_direct_tax_rate' field widget.
 *
 * @FieldWidget(
 *   id = "commerce_direct_tax_field_direct_tax_rate",
 *   label = @Translation("Direct Tax Rate"),
 *   field_types = {"commerce_direct_tax_field_tax_rate"},
 *   multiple_values = TRUE,
 * )
 */
class DirectTaxRateWidget extends WidgetBase implements ContainerFactoryPluginInterface {

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $plugin_id,
      $plugin_definition,
      $configuration['field_definition'],
      $configuration['settings'],
      $configuration['third_party_settings'],
      $container->get('entity_type.manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function __construct(
    $plugin_id,
    $plugin_definition,
    FieldDefinitionInterface $field_definition,
    array $settings,
    array $third_party_settings,
    EntityTypeManagerInterface $entityTypeManager
  ) {
    parent::__construct($plugin_id, $plugin_definition, $field_definition, $settings, $third_party_settings);
    $this->entityTypeManager = $entityTypeManager;
  }

  /**
   * {@inheritdoc}
   */
  public function formElement(FieldItemListInterface $items, $delta, array $element, array &$form, FormStateInterface $form_state) {
    $taxTypeId = $this->getFieldSetting('tax_type');
    /** @var \Drupal\commerce_tax\Entity\TaxType $taxType */
    $taxType = $this->entityTypeManager->getStorage('commerce_tax_type')->load($taxTypeId);
    $rates = $taxType->getPluginConfiguration()['rates'];

    $options = [
      '_none' => $this->t('Default'),
    ];
    foreach ($rates as $rate) {
      $options[$rate['id']] = ($rate['percentage'] * 100) . '% - ' . $rate['label'];
    }

    $element['tax_rate'] = $element + [
      '#type' => 'select',
      '#options' => $options,
      '#default_value' => isset($items[$delta]->tax_rate) ? $items[$delta]->tax_rate : NULL,
    ];

    return $element;
  }

}
