<?php

namespace Drupal\commerce_direct_tax_field\Resolver;

use Drupal\commerce_order\Entity\OrderItemInterface;
use Drupal\commerce_tax\Resolver\TaxRateResolverInterface;
use Drupal\commerce_tax\TaxZone;
use Drupal\profile\Entity\ProfileInterface;
use Drupal\commerce_direct_tax_field\MonolithicTaxRate;
use Drupal\Core\Entity\EntityTypeManagerInterface;

/**
 * Returns the tax zone's default tax rate.
 */
class ProductTaxResolver implements TaxRateResolverInterface {

  /**
   * Entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Constructor.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   *   Entity type manager.
   */
  public function __construct(
    EntityTypeManagerInterface $entityTypeManager
  ) {
    $this->entityTypeManager = $entityTypeManager;
  }

  /**
   * {@inheritdoc}
   */
  public function resolve(TaxZone $zone, OrderItemInterface $order_item, ProfileInterface $customer_profile) {
    $rates = $zone->getRates();

    // Get the purchased entity.
    $item = $order_item->getPurchasedEntity();
    if (!$item) {
      return NULL;
    }
    // Get Entity field definitions.
    $fieldDefinitions = $item->getFields(FALSE);
    // Find the first direct tax field.
    foreach ($fieldDefinitions as $fieldName => $fieldDefinition) {
      $fieldType = $fieldDefinition->getFieldDefinition()->getType();
      if ($fieldType == 'commerce_direct_tax_field_tax_rate') {
        // Get the rate value.
        if ($item->{$fieldName}->count() > 0) {
          $taxRateUuid = $item->{$fieldName}->first()->tax_rate;
          $taxTypeId = $fieldDefinition->getFieldDefinition()->getSetting('tax_type');
          /** @var \Drupal\commerce_tax\Entity\TaxType $taxType */
          $taxType = $this->entityTypeManager
            ->getStorage('commerce_tax_type')
            ->load($taxTypeId);

          $rates = $taxType->getPluginConfiguration()['rates'];
          foreach ($rates as $rate) {
            if ($rate['id'] == $taxRateUuid) {
              return new MonolithicTaxRate($rate);
            }
          }
        }
      }
    }

    // If no rate has been found, let's others resolvers try to get it.
    return NULL;
  }

}
